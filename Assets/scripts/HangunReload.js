﻿var ReloadSound : AudioSource;
var CrossObject : GameObject;
var MechanicsObject : GameObject;
var ClipCount : int;
var ReserveCount : int;
var ReloadAvailable : int;

function Update () {
	ClipCount = globalammo.LoadedAmmo;
	ReserveCount = globalammo.CurrentAmmo;

	if (ReserveCount == 0) {
		ReloadAvailable = 0;
	}
	else {
		ReloadAvailable = 10 - ClipCount;
	}

	if(Input.GetButtonDown("Reload")) {
		if (ReloadAvailable >= 1) {
			if (ReserveCount <= ReloadAvailable) {
				globalammo.LoadedAmmo += ReserveCount;
				globalammo.CurrentAmmo -= ReserveCount;
				ActionReload();
			}
			else {
				globalammo.LoadedAmmo += ReloadAvailable;
				globalammo.CurrentAmmo -= ReloadAvailable;
				ActionReload();
			}
		}
	EnableScripts();

	}
}

function EnableScripts () {
	yield WaitForSeconds (1.1);
			this.GetComponent("gunfire").enabled=true;
			CrossObject.SetActive(true);
			MechanicsObject.SetActive(true);
}

function ActionReload () {
	this.GetComponent("gunfire").enabled=false;
	CrossObject.SetActive(false);
	MechanicsObject.SetActive(false);
	ReloadSound.Play();
	GetComponent.<Animation>().Play("HandgunReload");
}